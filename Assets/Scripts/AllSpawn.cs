﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllSpawn : MonoBehaviour
{
    [SerializeField]
    private GameObject apple;
    [SerializeField]
    private GameObject nut;
    [SerializeField]
    private GameObject grape;
    [SerializeField]
    private GameObject mushroom;
    [SerializeField]
    private GameObject pear;
    [SerializeField]
    private GameObject verticalGrid;
    public static List<Vector2> positions = new List<Vector2>();
    public static List<Vector2> matchList = new List<Vector2>();
    public static List<Item> Items = new List<Item>();
    public static List<Vector2> destroyedObj = new List<Vector2>();
    public static GameObject[,] cellsWithObj = new GameObject[10, 20];
    private GameObject testObject;


    void Start()
    {

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                positions.Add(new Vector2(i, j));
                matchList.Add(new Vector2(i, j));
            }
        }
        for (int i = 0; i < 11; i++)
        {
            Instantiate(verticalGrid, new Vector2(-0.4f + i, 12), Quaternion.identity);
        }

    }

    void FixedUpdate()
    {
        if (matchList.Count > 0)
        {
            FirstSpawn();
        }
        else
        {

            foreach (var pos in positions)
            {
                int indexX = (int)(pos.x);
                int indexY = (int)(pos.y);
                Item objToList = new Item();
                objToList.objOnField = Instantiate(cellsWithObj[indexX, indexY], pos, Quaternion.identity) as GameObject;
                Items.Add(objToList);
            }
            positions.Clear();

        }
    }

    void FirstSpawn()
    {
        var listOfObjects = new List<GameObject> { apple, nut, grape, mushroom, pear };
        System.Random random = new System.Random();

        foreach (var pos in matchList)
        {
            int indexX = (int)(pos.x);
            int indexY = (int)(pos.y);
            int numberOfObj = random.Next(listOfObjects.Count);
            cellsWithObj[indexX, indexY] = listOfObjects[numberOfObj];
        }
        
        matchList.Clear();
        TestMatch();        
        //Debug.Log(matchList.Count + " MATCHES!!!");
    }

    void TestMatch()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 20; j++)
            {
                int x = i;
                int y = j;

                if (i < 2 && j > 1)
                {
                    if ((cellsWithObj[i, j].name == cellsWithObj[i, j - 1].name && cellsWithObj[i, j].name == cellsWithObj[i, j - 2].name))
                    {
                        matchList.Add(new Vector2(x, y));
                    }
                }
                if (j < 2 && i > 1)
                {
                    if (cellsWithObj[i, j].name == cellsWithObj[i - 1, j].name && (cellsWithObj[i, j].name == cellsWithObj[i - 2, j].name))
                    {
                        matchList.Add(new Vector2(x, y));
                    }
                }
                if (i > 1 && j > 1)
                {
                    if ((cellsWithObj[i, j].name == cellsWithObj[i - 1, j].name && (cellsWithObj[i, j].name == cellsWithObj[i - 2, j].name)) ||
                    (cellsWithObj[i, j].name == cellsWithObj[i, j - 1].name && cellsWithObj[i, j].name == cellsWithObj[i, j - 2].name))
                    {
                        matchList.Add(new Vector2(x, y));
                    }
                }

            }
        }

    }
}
