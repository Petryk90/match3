﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountPoints : MonoBehaviour {


    public Matches TestStartMatches = new Matches();
    private static int[] col = new int[10];
    private static GameObject[,] game = new GameObject[10, 20];
    private static IEnumerator coroutine;
    private static IEnumerator WaitAndPrint(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            print("WaitAndPrint " + Time.time);
        }
    }

    public static void FinedMatch()
    {
        int points;
        points = 3*Matches.matchesResult;
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                col[i] = col[i] + Matches.matchesIndex[i, j];
            }
            //Debug.Log("COL: "+"["+ i +"] = " + col[i]);
        }

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (Matches.matchesIndex[i, j] == 1)
                {
                    foreach (var item in AllSpawn.Items)
                    {
                        if ((item.XPos == i && item.YPos == j) && (Matches.matchesIndex[i, j] == 1))
                        {
                            Destroy(item.objOnField);
                            //AllSpawn.Items.Remove(item);
                        }
                    }
                }
            }
        }
        //coroutine = WaitAndPrint(2.0f);
        //Coroutine start;

        //Coroutine.

        //start.StartCoroutine(coroutine);

        for (int i = 0; i < 10; i++)
        {
            for (int j = 20-col[i]; j < 20; j++)
            {
                AllSpawn.matchList.Add(new Vector2(i, j));
                AllSpawn.positions.Add(new Vector2(i, j));
                Debug.Log(new Vector2(i, j));
            }
        }



        Matches.TestMatch();
    }
}
