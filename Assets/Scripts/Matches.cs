﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matches
{
    private static string[,] matchesType = new string[10, 10];
    public static int[,] matchesIndex = new int[10, 10];
    public static int matchesResult;
    public static bool isMatch;
    private static Item[,] transferObj = new Item[10, 20];


    public static void TestMatch()
    {
        isMatch = false;
        matchesResult = 0;

        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                foreach (var item in AllSpawn.Items)
                {
                    if (item.XPos == i && item.YPos == j)
                    {
                        transferObj[i, j] = item;
                    }
                }
                matchesType[i, j] = (string)(transferObj[i, j].objOnField.name);

                if (i < 2 && j > 1)
                {
                    if (matchesType[i, j] == matchesType[i, j - 1] && matchesType[i, j] == matchesType[i, j - 2])
                    {
                        matchesIndex[i, j] = 1;
                        matchesIndex[i, j - 1] = 1;
                        matchesIndex[i, j - 2] = 1;
                    }

                }
                if (j < 2 && i > 1)
                {
                    if (matchesType[i, j]  == matchesType[i - 1, j]  && (matchesType[i, j]  == matchesType[i - 2, j] ))
                    {
                        matchesIndex[i, j] = 1;
                        matchesIndex[i - 1, j] = 1;
                        matchesIndex[i - 2, j] = 1;
                    }

                }
                if (i > 1 && j > 1)
                {
                    if (matchesType[i, j]  == matchesType[i - 1, j]  && (matchesType[i, j]  == matchesType[i - 2, j] ))
                    {
                        matchesIndex[i, j] = 1;
                        matchesIndex[i - 1, j] = 1;
                        matchesIndex[i - 2, j] = 1;
                    }
                    if(matchesType[i, j] == matchesType[i, j - 1] && matchesType[i, j] == matchesType[i, j - 2])
                    {
                        matchesIndex[i, j] = 1;
                        matchesIndex[i, j - 1] = 1;
                        matchesIndex[i, j - 2] = 1;
                    }
                    
                }
            }
        }
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                matchesResult = matchesResult + matchesIndex[i, j];
            }
        }
        if (matchesResult>0)
        {
            isMatch = true;
        }
        //matchesResult = 0;
        //Debug.Log(isMatch);
    }
}

