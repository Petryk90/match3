﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public GameObject objOnField;

    public Vector2 ObjectPosition
    {
        get { return objOnField.transform.position; }
        set { value = objOnField.transform.position; }
    }

    public int XPos
    {
        get { return (int)objOnField.transform.position.x; }
        set { value = (int)objOnField.transform.position.x; }
    }

    public int YPos
    {
        get { return (int)objOnField.transform.position.y; }
        set { value = (int)objOnField.transform.position.y; }
    }

}
