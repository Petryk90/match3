﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ObjectController : MonoBehaviour
{


    [SerializeField]
    private GameObject backSellected, sellectedObj;
    private Item[,] transferObj = new Item[10, 20];
    private bool selected = false, hiton = false;
    private Vector2 sellectedVector, newVector;
    //public Matches TestStartMatches = new Matches();
    private int iPos, jPos, iPosNew, jPosNew, imgType;

    void ChangePosition()
    {
        transferObj[iPos, jPos].objOnField.transform.position = newVector;
        transferObj[iPosNew, jPosNew].objOnField.transform.position = sellectedVector;
        Matches.TestMatch();
        if (!Matches.isMatch)
        {
            transferObj[iPos, jPos].objOnField.transform.position = sellectedVector;
            transferObj[iPosNew, jPosNew].objOnField.transform.position = newVector;
        }
        else
        {
            CountPoints.FinedMatch();
        }

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 origin = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    foreach (var item in AllSpawn.Items)
                    {
                        if (item.XPos == i && item.YPos == j)
                        {
                            transferObj[i, j] = item;
                        }
                    }
                    //Debug.Log("Objects on positions: " + AllSpawn.cellsWithObj[i, j].transform.position);
                    //Debug.Log("Touch on positions: " + origin);
                    if (Vector2.Distance(origin, transferObj[i,j].ObjectPosition) < 0.6f)
                    {
                        hiton = true;
                        //Debug.Log(transferObj[i, j].objOnField.name);
                        iPosNew = i;
                        jPosNew = j;
                    }
                }
            }

            //Debug.Log(origin);
            if (hiton)
            {
                if (selected)
                {
                    newVector = new Vector2(iPosNew, jPosNew); 
                    if (((sellectedVector.x == newVector.x - 1) && (sellectedVector.y == newVector.y)) || ((sellectedVector.x == newVector.x + 1) && (sellectedVector.y == newVector.y)) || ((sellectedVector.y == newVector.y - 1) && (sellectedVector.x == newVector.x)) || ((sellectedVector.y == newVector.y + 1) && (sellectedVector.x == newVector.x)))
                    {
                        transferObj[iPos, jPos].objOnField.GetComponent<SpriteRenderer>().color = Color.white;
                        selected = false;
                        ChangePosition();
                    }
                    else
                    {
                        transferObj[iPos, jPos].objOnField.GetComponent<SpriteRenderer>().color = Color.white;
                        transferObj[iPosNew, jPosNew].objOnField.GetComponent<SpriteRenderer>().color = Color.green;
                        sellectedVector = new Vector2(iPosNew, jPosNew); 
                        iPos = iPosNew;
                        jPos = jPosNew;
                        selected = true;
                    }

                }
                else
                {
                    sellectedVector = new Vector2(iPosNew, jPosNew);
                    transferObj[iPosNew, jPosNew].objOnField.GetComponent<SpriteRenderer>().color = Color.green;
                    //transferObj[iPosNew, jPosNew].objOnField.transform.position = new Vector2(10 , 0);
                    iPos = iPosNew;
                    jPos = jPosNew;
                    selected = true;
                }


            }
        }
    }

}
